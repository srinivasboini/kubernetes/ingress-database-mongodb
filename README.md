# ingress-database-mongodb


## Add bitnami repo

```
helm repo add bitnami https://charts.bitnami.com/bitnami  

```


## mongodb template using helm

```
helm search repo bitnami/mongodb --versions


 helm template bitnami/mongodb  \
 --set fullnameOverride=mongodb \
 --set global.namespaceOverride=database-ns \
 --set auth.rootPassword=root \
 --set auth.username=srinivas \
 --set auth.password=password \
 --set auth.database=my_database > mongodb.yaml
```
